# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=libratbag tag=${PV} ]
require python [ blacklist=2 multibuild=false ]
require meson
require gtk-icon-cache

export_exlib_phases src_prepare

SUMMARY="GTK application to configure gaming mice"

LICENCES="GPL-2"
SLOT="0"

MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/libratbag[>=0.9]
        dev-python/evdev[python_abis:*(-)?]
        dev-python/lxml[python_abis:*(-)?]
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        x11-libs/gtk+:3[gobject-introspection]
"

piper_src_prepare() {
    edo pushd "${MESON_SOURCE}"

    # We'll update the icon cache, no need for meson to do it
    edo sed "/gtk-update-icon-cache/d" -i meson_install.sh

    # use correct python libdir
    edo sed -e "s: python_dir):'$(python_get_sitedir)'):" -i \
        meson.build

    # Don't use /bin/python3 but /bin/python$(python_get_abi)
    edo sed -e "s:env python3:env python$(python_get_abi):" -i \
        piper.in

    edo popd
}

