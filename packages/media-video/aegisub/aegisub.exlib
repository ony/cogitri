# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'aegisub-3.2.2_p20160518-r2.ebuild' from Gentoo, which is:
#     Copyright 1999-2017 Gentoo Foundation

SCM_EXTERNAL_REFS="vendor/boost: vendor/ffmpeg: vendor/ffms2: vendor/fftw:
    vendor/freetype2: vendor/googletest: vendor/icu: vendor/libass: vendor/uchardet: vendor/wxWidgets:"

require github [ user=Aegisub project=Aegisub tag=v${PV} ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require gtk-icon-cache freedesktop-desktop

export_exlib_phases src_prepare pkg_postrm pkg_postinst

SUMMARY="Aegisub is a free, cross-platform open source tool for creating and modifying subtitles"
HOMEPAGE="http://www.aegisub.org/"

LICENCES="BSD-3 MIT"
SLOT="0"

MYOPTIONS="
    openal
"

DEPENDENCIES="
    build:
        dev-util/intltool
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
    build+run:
        app-spell/hunspell:=[>=1.2.0]
        dev-libs/boost[>=1.50.0]
        dev-libs/icu:=[>=4.8.1.1]
        media-libs/ffms2[>=2.16]
        media-libs/fontconfig[>=2.4]
        media-libs/freetype:2
        media-libs/libass[fontconfig][>=0.9.7]
        net-misc/curl[>=7.18.2]
        sci-libs/fftw[>=3.3.0]
        sys-sound/alsa-lib [[ note = [ kfails to build without alsa ] ]]
        x11-libs/wxGTK:3.0[>=3.0.0]
        openal? ( media-libs/openal )
    suggestion:
        sys-sound/alsa-plugins [[ description = [ for sound with pulseaudio ] ]]
"

if ever is_scm; then
    DEPENDENCIES+="
        build+run:
            dev-lang/LuaJIT[>=2.0.5] [[ note = [ 2.0.5 introduces lua52compat ] ]]
    "
else
    DEPENDENCIES+="
        build+run:
            dev-lang/LuaJIT[>=2.0]
    "
fi

# requires unpackaged luarocks, busted
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-update-checker
    --with-alsa
    --with-ffms2
    --with-fftw3
    # Pulseaudio is pretty much broken
    --without-libpulse
    --without-oss
    --without-portaudio
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    openal
)

aegisub_src_prepare() {
    edo sed -e "s/BIN_AR       = ar/BIN_AR = ${AR}/" -i Makefile.inc.in
    edo sed -e "s/BIN_RANLIB   = ranlib/BIN_RANLIB = ${RANLIB}/" -i Makefile.inc.in

    autotools_src_prepare
}

aegisub_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

aegisub_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

